import React, { Component } from "react";
import Table from "./Table";

export class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      person: [
        {
          id: 1,
          email: "domZz@gmail.com",
          name: "domZz",
          age: 22,
          action: "Pending",
        },
      ],
      newEmail: "null",
      newName: "null",
      newAge: "null",
    };
  }
  handleEmail = (event) => {
    if (event.target.value != "") {
      this.setState({
        newEmail: event.target.value,
      });
    } else {
      this.setState({
        newEmail: "null",
      });
    }
  };
  handleUsername = (event) => {
    if (event.target.value != "") {
      this.setState({
        newName: event.target.value,
      });
    } else {
      this.setState({
        newName: "null",
      });
    }
  };
  handleAge = (event) => {
    if (event.target.value != "") {
      this.setState({
        newAge: event.target.value,
      });
    } else {
      this.setState({
        newAge: "null",
      });
    }
  };
  onSubmit = (event) => {
    event.preventDefault();
    const newObj = {
      id: this.state.person.length + 1,
      email: this.state.newEmail,
      name: this.state.newName,
      age: this.state.newAge,
      action: "Pending",
    };
    this.setState(
      {
        person: [...this.state.person, newObj],
        newPerson: "",
      },
      () => console.log("New", this.state.person)
    );
  };

  action = (event) => {
    let action = this.state.person;
    action.map((index) => {
      if (event.id == index.id) {
        index.action = index.action == "Pending" ? "Done" : "Pending";
      }
    });
    this.setState({
      ...this.state.person,
    });
  };

  render() {
    return (
      <div class="justify-center align-center flex">
        <div>
          <div class="w-full mt-10">
            <div class="w-full justify-center align-center flex">
              <div>
                <h1 class="text-4xl font-poppin text-center text-rose-700 font-bold">
                  Please fill your information
                </h1>
                <form class=" full font-poppin">
                  <div class="ml-2 gap-2 flex text-rose-700">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3"
                      />
                    </svg>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M21.75 6.75v10.5a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V6.75m19.5 0A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25m19.5 0v.243a2.25 2.25 0 01-1.07 1.916l-7.5 4.615a2.25 2.25 0 01-2.36 0L3.32 8.91a2.25 2.25 0 01-1.07-1.916V6.75"
                      />
                    </svg>
                  </div>
                  <div class="mb-3 mt-2 relative">
                    <input
                      onChange={this.handleEmail}
                      type="text"
                      id="floating"
                      placeholder=" "
                      class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    />
                    <label
                      for="floating"
                      class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
                    >
                      Your email
                    </label>
                  </div>
                  <div class="ml-2 gap-2 flex text-rose-700">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3"
                      />
                    </svg>

                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                  </div>
                  <div class="mb-3 mt-2 relative ">
                    <input
                      onChange={this.handleUsername}
                      type="text"
                      id="floating"
                      class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                    />
                    <label
                      for="floating"
                      class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
                    >
                      Username
                    </label>
                  </div>
                  <div class="ml-2 gap-2 flex text-rose-700">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3"
                      />
                    </svg>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6 "
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                      />
                    </svg>
                  </div>
                  <div class="mb-3 mt-2 relative ">
                    <input
                      onChange={this.handleAge}
                      type="number"
                      id="floating"
                      class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                      placeholder=" "
                    />
                    <label
                      for="floating"
                      class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
                    >
                      Age
                    </label>
                  </div>
                  <div class="mb-3 mt-2 align-middle justify-center flex">
                    <button
                      onClick={this.onSubmit}
                      type="submit"
                      class="relative rounded px-5 py-2.5 overflow-hidden group bg-rose-700 hover:bg-gradient-to-r hover:from-rose-700 hover:to-rose-700 text-white hover:ring-2 hover:ring-offset-2 hover:ring-rose-700 transition-all ease-out duration-300"
                    >
                      <span class="absolute right-0 w-8 h-32 -mt-12 transition-all duration-1000 transform translate-x-12 bg-white opacity-20 rotate-12 group-hover:-translate-x-40 ease"></span>
                      <span class="relative">Register</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <Table users={this.state.person} action={this.action} />
        </div>
      </div>
    );
  }
}

export default Form;
