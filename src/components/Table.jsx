import React, { Component } from "react";
import Swal from "sweetalert2";
import "animate.css";

export class Table extends Component {
  button = (item) => {
    Swal.fire({
      title: `Id: ${item.id} \n Email: ${item.email} \n Username: ${item.name} \n Age: ${item.age}`,
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
  };
  render() {
    return (
      <div class="w-[800px] relative shadow-md rounded-lg">
        <table>
          <thead class="w-full text-sm text-white font-poppin bg-rose-700">
            <tr>
              <th class="w-5">Id</th>
              <th class="w-52">Email</th>
              <th class="w-52">Username</th>
              <th class="w-10">Age</th>
              <th class="w-72">Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users.map((user) => (
              <tr
                class={
                  user.id % 2 == 0
                    ? "bg-[#FBD5D5] font-poppin"
                    : "bg-white font-poppin"
                }
              >
                <td class="px-6 py-4">{user.id}</td>
                <td class="px-6 py-4">{user.email}</td>
                <td class="px-6 py-4">{user.name}</td>
                <td class="px-6 py-4">{user.age}</td>
                <td class="px-6 py-4">
                  <div class="gap-5 align-middle justify-center flex">
                    <button
                      onClick={() => this.props.action(user)}
                      class={
                        user.action == "Pending"
                          ? "relative rounded px-5 py-2.5 overflow-hidden group bg-rose-700 hover:bg-gradient-to-r hover:from-rose-700 hover:to-rose-700 text-white hover:ring-2 hover:ring-offset-2 hover:ring-rose-700 transition-all ease-out duration-300"
                          : "relative rounded px-5 py-2.5 overflow-hidden group bg-lime-500 hover:bg-gradient-to-r hover:bg-lime-500 hover:to-lime-500 text-white hover:ring-2 hover:ring-offset-2 hover:ring-lime-500 transition-all ease-out duration-300"
                      }
                    >
                      <span class="absolute right-0 w-8 h-32 -mt-5 transition-all duration-1000 transform translate-x-12 bg-white opacity-20 rotate-12 group-hover:-translate-x-40 ease"></span>
                      <span class="relative">{user.action}</span>
                    </button>
                    <button
                      onClick={() => this.button(user)}
                      class="relative rounded px-5 py-2.5 overflow-hidden group bg-blue-700 hover:bg-gradient-to-r hover:from-blue-700 hover:to-blue-700 text-white hover:ring-2 hover:ring-offset-2 hover:ring-blue-700 transition-all ease-out duration-300"
                    >
                      <span class="absolute right-0 w-8 h-32 -mt-5 transition-all duration-1000 transform translate-x-12 bg-white opacity-20 rotate-12 group-hover:-translate-x-40 ease"></span>
                      <span class="relative">Detail</span>
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
